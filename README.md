# `msh`: music shell

msh is a music player used through a command line. It uses
[cmd2](https://github.com/python-cmd2/cmd2) for the CLI, which allows for
example to pipe internal msh commands into external commands, e.g.
`playlist show | sort` will sort the playlist using the sort UNIX utility.

## Installation

Python strict dependencies (pip package names):
- [python-vlc](https://github.com/oaubert/python-vlc)
- [cmd2](https://github.com/python-cmd2/cmd2)
- [platformdirs](https://github.com/platformdirs/platformdirs)

You also need to install libvlc, for the python VLC bindings to work.

Optional dependencies, allowing streaming:
- [yt-dlp](https://github.com/yt-dlp/yt-dlp): used to stream videos from YouTube
  and the likes (only audio is played)

Installation using pip should work: run
`pip install 'git+https://gitlab.com/ewenak/msh'` or clone this repository and
run `pip install .`

To install msh with the optional requirements for streaming:
`pip install 'msh[streaming] @ git+https://gitlab.com/ewenak/msh'` or clone this
repository and run `pip install .[streaming]`

## Using msh

The `help` command will show a list of the supported commands, including `msh`
commands and `cmd2` built-in commands. Running `help COMMAND` will show the
help text for `COMMAND`.

You can add tracks to the playlist by running `add FILENAME`. To start listening
to your playlist, run `play`. You can toggle the paused state with the `pause`
command.

msh can scan directories to let you search music tracks by their metadata. Run
`scan DIRECTORY` to search for music files in that directory. You can the search
your media library using the `search`.

There are different options that can be set using the `set` command. Run it
without any arguments to see the list of available options.

If you installed yt-dlp, you can add online videos to the playlist by running
`add URL`. Search YouTube with `yt search QUERY`, or `yt info URL_OR_QUERY` if
you want to see the title / channel and other information rather than just
URLs.
