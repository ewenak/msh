#! /usr/bin/env python3
"""Music shell"""

import os
import sys

import locale
import readline

try:
    import vlc
except ImportError:
    print("""Couldn't import vlc module. Make sure it is installed, as well as
vlc library.
It can be installed on debian-based systems with:
    sudo apt install python3-vlc""")
    sys.exit(1)

try:
    import yt_dlp  # NOQA: F401
except ImportError:
    print("""Couldn't import yt_dlp module.
It can be installed on debian-based systems with:
    sudo apt install yt-dlp""")

from .settings import (MSHRC_FILENAME, STATE_FILENAME,
                       TAGSDB_FILENAME, VLC_LOGFILE)

from .musiccli import MusicCLI
from .list_player import ListPlayer
from .tagsdb import TagsDB

readline.set_completer_delims(' \t\n')

locale.setlocale(locale.LC_ALL, '')  # Use '' for auto

# It will be defined at the end, to use .cache directory
instance = None

if not os.path.exists(MSHRC_FILENAME):
    with open(MSHRC_FILENAME, 'w') as f:
        f.write('''alias create ls shell ls --color=auto
alias create la shell ls --color=auto -A
alias create ll shell ls --color=auto -l
''')

tags_db = TagsDB(TAGSDB_FILENAME)

instance = vlc.Instance(f'--novideo -q 2> { VLC_LOGFILE }')

list_player = ListPlayer(instance, tags_db)

musiccli = MusicCLI(list_player, tags_db, STATE_FILENAME,
                    startup_script=MSHRC_FILENAME, silence_startup_script=True)


def main():
    for el in sys.argv[1:]:
        list_player.add(os.path.abspath(el))
    musiccli.cmdloop()
