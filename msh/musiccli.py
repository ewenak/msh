#! /usr/bin/env python3

import argparse
import cmd2
import datetime
import functools
import glob
import json
import os
from pathlib import Path
import random
import re
import subprocess
import tempfile
import traceback
import urllib
import vlc

from .list_player import ALLOWED_AT_END_ACTIONS
from .server import run_server
from .settings import DOWNLOADED_VIDEOS_DIR
from .tools import (add_slash_if_dir, human_format, secs_to_hh_mm_ss,
                    ytdl_extract_info, get_local_media_info,
                    set_local_media_info, youtube_suggestions, volume_value,
                    METADATA_STRING_ENUM_MAPPING)
variables = {
    'PS1': '> ',
    'PATH': os.getcwd(),
    'USER': os.environ.get('USERNAME', '')
}


class FileGlobAction(argparse._StoreAction):
    """Argparse action for glob pattern matching.
    It applies glob.glob (with recursive set to True, it allows ** to traverse
    directories) then sorts the result.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_completer(cmd2.Cmd.path_complete)

    def __call__(self, parser, namespace, values, option_string=None):
        files = []
        for pattern in values:
            if pattern.startswith('file://'):
                filename = urllib.parse.unquote(pattern[len('file://'):])
                files.append(filename)
            elif re.match(r'^\w+://.+$', pattern):
                # It's some kind of URL
                files.append(pattern)
            else:
                f = glob.glob(os.path.expanduser(pattern))
                if len(f) > 0:
                    files += sorted(f)
                else:
                    files.append(pattern)
        super().__call__(parser, namespace, files, option_string)


class MusicCLI(cmd2.Cmd):
    def __init__(self, list_player, tags_db, state_file, *args, options=None,
                 **kwargs):
        self.prompt = '> '
        self.list_player = list_player
        self.tags_db = tags_db
        super().__init__(*args, **kwargs)

        # Commands that can't be run through the HTTP server
        self.http_allowed_commands = [
            'add', 'file', 'help', 'next', 'play', 'playlist', 'pause',
            'previous', 'scan', 'search', 'sort', 'status', 'stop', 'volume',
            'yt',
        ]
        self.httpd = None

        self.jumpplaylistedit = True
        self.playmode = self.list_player.at_end_action
        self.randomtrackpath = self.list_player.random_track_path_pattern
        self.savestate = False
        self.httpserver = False

        def play_mode_converter(value: str):
            lower_value = value.lower()
            if lower_value not in ALLOWED_AT_END_ACTIONS:
                raise ValueError(
                    "must be one of: "
                    f"{', '.join(repr(a) for a in ALLOWED_AT_END_ACTIONS)}"
                    "(case-insensitive)"
                )
            return lower_value

        self.add_settable(cmd2.Settable(
            'savestate', bool, 'Save state to file on exit', self))
        self.add_settable(cmd2.Settable(
            'httpserver', bool, 'Launch an HTTP server to control MSH', self,
            onchange_cb=self.check_httpserver
        ))
        self.add_settable(cmd2.Settable(
            'playmode', play_mode_converter, 'Behavior at the end of the '
            'playlist ("stop", "repeat" or "random")', self,
            onchange_cb=self.check_play_mode,
        ))
        self.add_settable(cmd2.Settable(
            'randomtrackpath', str, 'SQL LIKE pattern to filter tracks to be '
            'added to the playlist when playmode is random', self,
            onchange_cb=self.check_randomtrackpath,
        ))
        self.add_settable(cmd2.Settable(
            'jumpplaylistedit', bool, 'Jump to currently played music when '
            'running playlist edit. Uses the editor +LINENUM FILE syntax, '
            'supported by most editors.', self))
        self.state_file = state_file
        if os.path.exists(self.state_file):
            with open(self.state_file, 'r') as f:
                saved_state = json.load(f)
            self.restore_state(saved_state)

        self.register_postloop_hook(self.on_exit)

    add_parser = cmd2.Cmd2ArgumentParser()
    add_parser.add_argument('-n', '--next',
                            action=argparse.BooleanOptionalAction,
                            help='Add files just after this track')
    add_parser.add_argument('-r', '--random',
                            action=argparse.BooleanOptionalAction,
                            help='Shuffle files before adding')
    add_parser.add_argument('files', nargs='+', action=FileGlobAction,
                            help='Files to be added to the playlist')

    @cmd2.with_argparser(add_parser)
    def do_add(self, args):
        """Add files to the playlist"""
        if args.random:
            random.shuffle(args.files)
        if args.next:
            self.list_player.add(*args.files, position='next')
        else:
            self.list_player.add(*args.files)

    complete_add = cmd2.Cmd.path_complete

    def do_play(self, line):
        """Play playlist"""
        self.list_player.play()

    def do_pause(self, line):
        """Pause"""
        self.list_player.pause()

    volume_parser = cmd2.Cmd2ArgumentParser()
    volume_parser.add_argument('action', nargs='?', choices='-+=', help='Set, '
                               'augment or disminish the volume')
    volume_parser.add_argument(
        'value', nargs='?', type=volume_value, help='Percentage to increment, '
        'set or decrement volume')

    @cmd2.with_argparser(volume_parser)
    def do_volume(self, args):
        """Get or set volume"""
        if args.action is None:
            print(self.list_player.volume)
            return
        if args.value is None:
            if args.action == '=':
                args.value = 100
            else:
                args.value = 5
        if args.action == '+':
            self.list_player.volume += args.value
        elif args.action == '-':
            self.list_player.volume -= args.value
        else:
            self.list_player.volume = args.value

    def do_status(self, line):
        """Show status and playing file's name"""
        status_message = {
            vlc.State.Buffering: 'Buffering',
            vlc.State.Ended: 'Ended',
            vlc.State.Error: 'Error',
            vlc.State.NothingSpecial: 'Nothing special',
            vlc.State.Opening: 'Opening',
            vlc.State.Paused: 'Paused',
            vlc.State.Playing: 'Playing',
            vlc.State.Stopped: 'Stopped',
        }
        state = self.list_player.player.get_state()
        print(status_message[state])
        if state in (vlc.State.Paused, vlc.State.Playing):
            media_info = (f'Playing { self.list_player.media_info["title"] }, '
                          f'in { self.list_player.media_info["album"] }, by '
                          f'{ self.list_player.media_info["artist"] } '
                          f'({ self.list_player.media_name })')
            print(media_info)
            duration = secs_to_hh_mm_ss(self.list_player.get_duration())
            time = secs_to_hh_mm_ss(self.list_player.get_time())
            used_space = len(duration) + len(time) + 4
            free_space = os.get_terminal_size(0)[0] - used_space
            position = self.list_player.player.get_position()
            full = free_space * position
            empty = free_space - full
            print(f"{ time } [{ '#' * round(full) }{ '.' * round(empty) }] "
                  f"{ duration }")

    def do_next(self, line):
        """Next file"""
        self.list_player.stop()
        self.list_player.next()

    def do_previous(self, line):
        """Previous file"""
        self.list_player.stop()
        self.list_player.previous()

    def do_stop(self, line):
        """Stop playing"""
        self.list_player.stop()

    def parse_time(self, time):
        if time[0] == '+':
            return self.list_player.get_time() + self.parse_time(time[1:])
        elif time[0] == '-':
            return self.list_player.get_time() - self.parse_time(time[1:])
        if time.endswith('%'):
            try:
                percent = float(time[:-1])
            except ValueError:
                return None
            return percent / 100 * self.list_player.get_duration()
        if 'h' in time or 'H' in time:
            match = re.match(
                r'^([\d.]+h)([\d.]+m?)?([\d.]+s?)?$', time, re.IGNORECASE)
            if match is None:
                return None
            hours, minutes, seconds = (
                float(n.strip('hmsHMS')) if n is not None else 0
                for n in match.groups()
            )
            return hours * 3600 + minutes * 60 + seconds
        else:
            match = re.match(r'^([\d.]+m)?([\d.]+s?)?$', time, re.IGNORECASE)
            if match is None:
                return None
            minutes, seconds = (
                float(n.strip('hmsHMS')) if n is not None else 0
                for n in match.groups()
            )
            return minutes * 60 + seconds

    @cmd2.with_argument_list()
    def do_goto(self, args):
        """goto -h
Usage: goto [-h] time

Set the music time

positional arguments:
  time        Set the music time to this time.
              It can be in the following formats:
               - SECONDS                   10 (absolute seconds)
               - [HOURh][MINm][SECs]       1h0m, 1m2, 2s, 1m2s (absolute)
               - PERCENTAGE%               50% (percentage of the duration of
                                           the currently played tune)
               - +TIME, -TIME              +10, -1m3s, -5% (relative to current
                                           position)

optional arguments:
  -h, --help  show this help message and exit

"""
        if len(args) == 0:
            print('Usage: goto [-h] time')
            self.perror('Error: the following arguments are required: time')
            return
        elif len(args) > 1:
            print('Usage: goto [-h] time')
            self.perror(f'Error: unrecognized arguments: {" ".join(args[1:])}')
            return
        elif args[0] == '-h':
            self.do_help('goto')
            return
        seconds = self.parse_time(args[0])
        if seconds is None:
            self.perror(f'Invalid time: {args[0]}')
            print('Run goto -h for details on how to format time.')
            return
        self.list_player.set_time(seconds)

    cd_parser = cmd2.Cmd2ArgumentParser()
    cd_parser.add_argument('directory', default='~', nargs='?',
                           help='Where to change directory')

    @cmd2.with_argparser(cd_parser)
    def do_cd(self, args):
        """Change directory"""
        d = add_slash_if_dir(os.path.expanduser(args.directory))
        if os.path.exists(d):
            if os.path.isdir(d):
                os.chdir(d)
                variables['PATH'] = os.getcwd()
            else:
                print(f'cd: { d } is not a directory')
        else:
            print(f'cd: { d } does not exist')
    complete_cd = functools.partialmethod(cmd2.Cmd.path_complete,
                                          path_filter=os.path.isdir)

    def do_pwd(self, line):
        """Print working directory"""
        print(os.getcwd())

    file_parser = cmd2.Cmd2ArgumentParser()
    file_parser.add_argument(
        '-i', '--index', nargs='+', default=[],
        help='Print metadata of the file at INDEX in the playlist')
    file_parser.add_argument('files', nargs='*', action=FileGlobAction,
                             help='Print metadata of these files')

    @cmd2.with_argparser(file_parser)
    def do_file(self, args):
        """Print track, title, genre, artist, album and duration of an audio
file"""
        if len(args.files) + len(args.index) == 0:
            args.index = ['current']

        for f in args.files + [
            self.list_player.media_list[self.parse_index(i)]
            for i in args.index
        ]:
            info = get_local_media_info(f)
            for key, value in info.items():
                if key != 'duration':
                    print(f'{ key.capitalize() }: { value }')
            if 'duration' in info:
                print('Duration:', secs_to_hh_mm_ss(info['duration']))
    complete_file = cmd2.Cmd.path_complete

    tags_parser = cmd2.Cmd2ArgumentParser(add_help=False)
    tags_subparsers = tags_parser.add_subparsers(
        title='subcommands', help='subcommand help')

    tags_show_parser = tags_subparsers.add_parser('show', help='Show metadata')
    tags_show_parser.add_argument(
        '-f', '--fields', nargs='+', default='',
        choices=tuple(METADATA_STRING_ENUM_MAPPING.keys()),
        help='Metadata fields to show (all by default)')
    tags_show_parser.add_argument(
        'files', metavar='FILES', action=FileGlobAction, nargs='+',
        help='Files on which to operate')

    tags_edit_parser = tags_subparsers.add_parser(
        'edit',
        help='Interactively edit metadata in your favourite text editor'
    )
    tags_edit_parser.add_argument(
        'files', metavar='files', action=FileGlobAction, nargs='+',
        help='files on which to operate')

    tags_set_parser = tags_subparsers.add_parser(
        'set', help='Modify fields from the command line')
    for field in METADATA_STRING_ENUM_MAPPING:
        tags_set_parser.add_argument(
            f'--{field.replace("_", "")}', dest=field, default=None,
            help=f'Value of the field {field.replace("_", "")}')
    tags_set_parser.add_argument(
        'files', metavar='files', action=FileGlobAction, nargs='+',
        help='files on which to operate')

    def tags_show(self, args):
        for filename in args.files:
            file_path = Path(filename)
            if not file_path.exists():
                print(f'{file_path} does not exist; skipping\n')
                continue
            elif not file_path.is_file():
                print(f'{file_path} is not a regular file; skipping\n')
                continue

            metadata = get_local_media_info(file_path)
            if len(args.fields) > 0:
                for field in args.fields:
                    print(f"{field.replace('_', ' ').capitalize()}: "
                          f"{metadata[field]}")
            else:
                for k, v in metadata.items():
                    if k == 'duration':
                        formatted_value = secs_to_hh_mm_ss(v)
                    else:
                        formatted_value = v
                    formatted_key = k.replace('_', ' ').capitalize()
                    print(f"{formatted_key}: {formatted_value}")
            print()

    tags_show_parser.set_defaults(func=tags_show)

    def tags_edit(self, args):
        with tempfile.NamedTemporaryFile(prefix='msh-tags-editor-',
                                         mode='w+t') as file:
            for media_filename in args.files:
                media_path = Path(media_filename)
                if not media_path.is_file():
                    self.pwarning(f'{file} does not exist or is not a regular '
                                  'file.')
                    continue

                metadata = get_local_media_info(media_path)
                for k, v in metadata.items():
                    if k == 'duration':
                        continue
                    file.write(f"{k.replace('_', ' ').capitalize()}: {v}\n")
                file.write('\n')

            file.flush()

            editor = self.editor
            if self.editor is None:
                editor = 'sensible-editor'
            try:
                subprocess.check_call([editor, file.name])
            except subprocess.CalledProcessError:
                self.perror('Error: there was a problem with the editor '
                            f'"{editor}"')
                return

            file.seek(0)
            current_filename = None
            metadata = {}
            for linenum, raw_line in enumerate(file.readlines()):
                if len(raw_line.strip()) == 0:
                    continue
                line = raw_line.rstrip('\n')

                if line.startswith('Filename: '):
                    if current_filename is not None:
                        set_local_media_info(current_filename, metadata)

                    current_filename = line[len('Filename: '):]
                    metadata = {}
                    continue
                elif current_filename is None:
                    self.perror(line)
                    self.perror(f'L{linenum}: expecting "Filename: "')
                    return

                key, sep, value = line.partition(': ')
                if sep == '':
                    self.perror(line)
                    self.perror(f'L{linenum}: syntax error')
                    return
                key = key.replace(' ', '_').lower()
                if key not in METADATA_STRING_ENUM_MAPPING:
                    self.perror(line)
                    self.perror(f'L{linenum}: Unknown metadata key: "{key}"')
                    return
                if value == '':
                    value = None
                metadata[key] = value
            if current_filename is not None:
                set_local_media_info(current_filename, metadata)

    tags_edit_parser.set_defaults(func=tags_edit)

    def tags_set(self, args):
        info = {}
        for field in METADATA_STRING_ENUM_MAPPING:
            if (val := getattr(args, field, None)) is not None:
                info[field] = val

        for file in args.files:
            if not set_local_media_info(file, info):
                self.pwarning(f"Metadata for {file} couldn't be saved.")

    tags_set_parser.set_defaults(func=tags_set)

    @cmd2.with_argparser(tags_parser)
    def do_tags(self, args):
        """Show or modify metadata tags of audio files."""
        # Try to get the subcommand's function
        func = getattr(args, 'func', None)
        if func is not None:
            func(self, args)
        else:
            # No subcommand, show help
            self.do_help('tags')

    scan_parser = cmd2.Cmd2ArgumentParser()
    scan_parser.add_argument('dirs', nargs='+', action=FileGlobAction,
                             help='Scan these directories')
    scan_parser.add_argument(
        '-i', '--interactive', action=argparse.BooleanOptionalAction,
        help='Stop on error retrieving metadata and ask before continuing')

    @cmd2.with_argparser(scan_parser)
    def do_scan(self, args):
        """Scan one or more directory(s)
Stores title, artist, album, track number, genre, duration and last modified
time in a database so that you can search for them"""
        for d in args.dirs:
            self.tags_db.scan(d, interactive=args.interactive)
    complete_scan = functools.partialmethod(cmd2.Cmd.path_complete,
                                            path_filter=os.path.isdir)

    search_parser = cmd2.Cmd2ArgumentParser(
        epilog='You can use SQL LIKE syntax in all fields: % means any string'
        'of characters and _ means any single character.')
    search_parser.add_argument(
        'query', default='%', nargs='?', help='Search queries (to be '
        'searched for in artist, album and title fields)')
    search_parser.add_argument(
        '-a', '--artist', default='%',
        help='Only show medias by an artist that contains ARTIST')
    search_parser.add_argument(
        '-c', '--command', nargs='?',
        help='Run msh command COMMAND with the results as parameters')
    search_parser.add_argument(
        '-t', '--title', default='%',
        help='Only show medias with a title that contains TITLE')
    search_parser.add_argument(
        '-A', '--album', default='%', help='Only show medias in an '
        'album that contains ALBUM')
    search_parser.add_argument(
        '-p', '--path', default='%',
        help='Only show medias with a path that matches the PATH')
    search_parser.add_argument(
        '-l', '--limit', default=None,
        help='Maximum number of returned tracks')

    @cmd2.with_argparser(search_parser)
    def do_search(self, args):
        """Search for title, artist or album"""
        results = [p[0] for p in self.tags_db.search(
            query=args.query, artist=args.artist, title=args.title,
            album=args.album, path=args.path, limit=args.limit)]
        if args.command is None:
            print('\n'.join(results))
        else:
            command = args.command + ' ' + ' '.join(
                cmd2.utils.quote_string(filename) for filename in results)
            self.onecmd(command)

    sort_factors = ['random', 'filename', 'track', 'title', 'genre', 'artist',
                    'album', 'duration']

    def complete_sort(self, text, line, begidx, endidx):
        return self.basic_complete(text, line, begidx, endidx,
                                   match_against=self.sort_factors)

    sort_parser = cmd2.Cmd2ArgumentParser()
    sort_parser.add_argument('key', nargs='+', choices=sort_factors,
                             help='Sort by KEY')

    @cmd2.with_argparser(sort_parser)
    def do_sort(self, args):
        """Sort playlist by track, title, genre, artist, album, duration,
filename or randomly"""
        if args.key == ['random']:
            def key(item):
                return random.random()
        else:
            def key(filename):
                tags = {
                    **get_local_media_info(filename),
                    'filename': filename,
                    'random': random.random()
                }
                return [tags.get(k.lower(), None) for k in args.key]

        self.list_player.media_list = sorted(self.list_player.media_list,
                                             key=key)

    def parse_index(self, index):
        # Inspired from
        # https://levelup.gitconnected.com/3-ways-to-write-a-calculator-in-python-61642f2e4a9a

        index_words = {'last': len(self.list_player.media_list) - 1,
                       'previous': self.list_player.media_list_index - 1,
                       'current': self.list_player.media_list_index,
                       'next': self.list_player.media_list_index + 1}
        index = index.strip()
        index_no_words = index
        for word, value in index_words.items():
            index_no_words = index_no_words.replace(word, str(value))

        if re.match(r'^-?\d+$', index_no_words):
            # always return positive numbers
            return max(int(index_no_words), 0)

        operators = {'+': lambda a, b: a + b, '-': lambda a, b: a - b}
        for o in operators:
            left, operator, right = index_no_words.partition(o)
            if operator in operators:
                left = self.parse_index(left)
                right = self.parse_index(right)
                if None in (left, right):
                    return None
                return operators[operator](left, right)
        return None

    def parse_index_range(self, index_range):
        try:
            start, end = index_range.split(':')
        except ValueError:
            index = self.parse_index(index_range)
            if index is not None:
                return (index, index)
            return (None, None)
        if start.startswith(('+', '-')) and end.startswith(('+', '-')):
            return (None, None)
        if start.startswith(('+', '-')):
            start = end + start
        elif end.startswith(('+', '-')):
            end = start + end
        start, end = self.parse_index(start), self.parse_index(end)
        return start, end

    @cmd2.with_argument_list
    def do_playlist(self, args):
        """Usage: playlist SUBCOMMAND [arguments...]

Playlist management commands

Subcommands:
  clear                 Entirely empty the playlist
  export FILENAME       Export the playlist in the m3u format in FILENAME
  edit                  Edit the playlist
                        It will launch an external editor (use `set editor` to
                        set which one) in a file with the list of the tracks
                        preceded by their rank in the playlist. Do not remove
                        the numbers, they are used to determine where the
                        currently played track is.
  index [NEWINDEX]      Display or set the index of the currently played track
                        in the playlist
  length                Display the length of the playlist
  move ORIG [DEST=next] Move track at ORIG index in the playlist to index DEST.
                        DEST defaults to next track.
  show [INDEXRANGE=0:last]
                        Print the filename of tracks in the playlist, line by
                        line

Indexes can be a number, "last", "current", "previous" or "next". They can
contain + and - operations.
  Example:
      next+3     current-7     0
Index ranges are in the form INDEX:INDEX and one of the two indexes can start
with a - or a +, and it will be evaluated relatively to the other INDEX.
  Example:
      current-7:current+7      current-7:+14     0:last

`playlist` alone means `playlist show current-7:current+7`
"""

        if '-h' in args or '--help' in args:
            self.do_help('playlist')
            return
        if len(args) == 0:
            args = ['show', 'current-7:current+7']
        command = args[0]
        if command == 'show':
            if len(args) == 1:
                args.append('0:last')
            start, end = self.parse_index_range(args[1])
            if start < 0:
                start = 0
            if end < 0:
                end = 0
            if start is None:
                print(f'Invalid index: "{start}"')
                return
            if end is None:
                print(f'Invalid index: "{end}"')
                return
            current_index = self.list_player.media_list_index - start
            for index, media in enumerate(
                    self.list_player.media_list[start:end + 1]):
                if index == current_index:
                    print(f'\033[1m{ media }\033[0m')
                else:
                    print(media)
        elif command == 'edit':
            with tempfile.NamedTemporaryFile(prefix='msh-playlist-',
                                             mode='w+t') as file:
                for i, filename in enumerate(self.list_player.media_list):
                    if i != self.list_player.media_list_index:
                        file.write(filename + '\n')
                    else:
                        file.write(f'! {filename}\n')
                file.flush()
                editor = self.editor
                if self.editor is None:
                    editor = 'sensible-editor'
                command = [
                    editor, f'+{self.list_player.media_list_index}', file.name]
                if not self.jumpplaylistedit:
                    command = [editor, file.name]
                try:
                    subprocess.check_call(command)
                except subprocess.CalledProcessError:
                    return

                # If at_end_action is 'random', some tracks will be added.
                old_at_end_action = self.list_player.at_end_action
                self.list_player.at_end_action = 'stop'

                file.seek(0)
                self.list_player.reset()
                for raw_line in file.readlines():
                    if raw_line.startswith('! '):
                        line = raw_line[2:].strip()
                        self.list_player._media_list_index = len(
                            self.list_player.media_list)
                    else:
                        line = raw_line.strip()

                    if line.startswith(('http://', 'https://')):
                        self.list_player.add(line)
                    else:
                        files = glob.glob(line)
                        if len(files) > 0:
                            self.list_player.add(*sorted(files))
                        else:
                            self.perror(f'{ line } file does not exist')
                self.list_player.at_end_action = old_at_end_action
        elif command == 'clear':
            self.list_player.reset()
            self.list_player.stop()
        elif command == 'export':
            if len(args) != 2:
                print('Usage: playlist export FILENAME')
                return

            filename = args[1]
            with open(filename, 'w') as f:
                f.write('\n'.join(self.list_player.media_list))

            print(f'Playlist saved as m3u in: { filename }')
        elif command == 'index':
            if len(args) == 1:
                print(self.list_player.media_list_index)
            elif len(args) == 2:
                index = self.parse_index(args[1])
                if index is None:
                    print(f'Invalid index: { index }')
                    return
                self.list_player.media_list_index = index
            else:
                print('Usage: playlist index INDEX')
        elif command == 'length':
            print(len(self.list_player.media_list))
        elif command == 'move':
            if not 1 < len(args) < 4:
                print('Usage: playlist move ORIGIN [DESTINATION=next]')
                return
            if len(args) == 2:
                args.append('next')
            origin = self.parse_index(args[1])
            destination = self.parse_index(args[2])
            if origin is None:
                print(f'Invalid index: "{args[1]}"')
                return
            if destination is None:
                print(f'Invalid index: "{args[2]}"')
                return
            if origin < destination:
                destination -= 1
            if (origin < self.list_player.media_list_index
                    and destination > self.list_player.media_list_index):
                self.list_player.media_list_index -= 1
            item = self.list_player.media_list.pop(origin)
            self.list_player.media_list.insert(destination, item)
        else:
            print(f'Invalid command { args[0] }')
            self.show_usage(self.do_playlist)

    do_playlist.commands = ['show', 'edit', 'clear', 'export', 'index',
                            'length']

    def complete_playlist(self, text, line, begidx, endidx):
        index_dict = {
            1: self.do_playlist.commands
        }
        return self.index_based_complete(text, line, begidx, endidx,
                                         index_dict=index_dict)

    @cmd2.with_argument_list
    def do_yt(self, args):
        """Usage: yt SUBCOMMAND [arguments...]

Search video websites, show information about some online video or download a
video using yt-dlp

Run `yt-dlp --list-extractors` in your shell for a list of supported websites.

Subcommands:
  download URL   Download the video at URL
  info URL...    Show information about the videos at URL
  search QUERY   Search YouTube for QUERY
                 You can use yt-dlp search prefixes.
"""
        if '-h' in args or '--help' in args:
            self.do_help('yt')
            return
        if len(args) == 0:
            self.show_usage(self.do_yt)
            return
        if args[0] == 'search':
            info = ytdl_extract_info(' '.join(args[1:]))
            if info is None:
                return

            if 'entries' in info:
                print('\n'.join([v.get('url', v.get('webpage_url',
                                                    'No video found'))
                                 for v in info['entries']]))
            elif 'webpage_url' in info:
                print(info['webpage_url'])
            else:
                print('No video found')
        elif args[0] == 'info':
            videos_info = ytdl_extract_info(' '.join(args[1:]))
            if videos_info is None:
                return

            # Data for playlist / search results is incomplete and does not use
            # the same keys for some values.
            playlist_url_mode = False

            if videos_info.get('_type') == 'playlist':
                videos_info = videos_info['entries']
                if videos_info[0].get('_type') == 'url':
                    playlist_url_mode = True
            else:
                videos_info = [videos_info]

            if self.debug:
                from copy import copy
                from pprint import pprint
                info_debug = copy(videos_info)
                for i in range(len(videos_info)):
                    for k in ['formats', 'thumbnails', 'automatic_captions',
                              'fragments', 'heatmap']:
                        info_debug[i][k] = None
                pprint(info_debug)

            for info in videos_info:
                if not playlist_url_mode:
                    print('Url:', info.get('webpage_url', 'No url'))
                else:
                    print('Url:', info.get('url', 'No url'))
                print('Title:', '\033[4m' + info.get('title', 'No title')
                      + '\033[0m')
                print('Channel:', info.get('channel', 'No channel'))
                if not playlist_url_mode:
                    print('Uploader:', info.get('uploader', 'No uploader'))
                    raw_upload_date = info.get('upload_date')
                    if raw_upload_date is not None:
                        upload_date = datetime.date(int(raw_upload_date[:4]),
                                                    int(raw_upload_date[4:6]),
                                                    int(raw_upload_date[6:]))
                        print('Upload date:', upload_date)
                    else:
                        print('Upload date: No upload date')
                if info.get('age_limit', '0') != '0':
                    print('Age limit:', info['age_limit'])
                if info.get('duration') is not None:
                    print('Duration:', secs_to_hh_mm_ss(info['duration']))
                else:
                    print('Duration: Unknown')
                if info.get('view_count') is not None:
                    print(f'Views: { human_format(info["view_count"]) }'
                          f'({ info["view_count"] :n})')
                else:
                    print('Views: Unknown')
                print('\033[3mDescription: ', end='')
                if info.get('description') is not None:
                    print(info['description'])
                else:
                    print('\033[0;1mNo description')
                print('\033[0m')
                print('\n' * 2)
        elif args[0] == 'download':
            for v in args[1:]:
                info = ytdl_extract_info(v, download=True)
                if info is None:
                    return
                pattern = f'{ DOWNLOADED_VIDEOS_DIR / info["id"] }.*'
                filename = glob.glob(pattern)
                if len(filename) < 1:
                    raise FileNotFoundError(
                        f'No file which matches the pattern "{ pattern }"')
                elif len(filename) > 1:
                    raise FileExistsError(f'More than one file which matches'
                                          f'the pattern "{ pattern }"')
                else:
                    filename = filename[0]
                print('Video stored in:', filename)
                title = info.get('title', info['webpage_url'])
                album = info.get('album', 'YouTube')
                artist = info.get('artist', '')
                track = info.get('track', '')
                genre = info.get('genre', 'YouTube')
                duration = info.get('duration', '')
                if artist == '':
                    artist = info.get('uploader', '')
                self.tags_db.add_file(filename, title, artist, album, track,
                                      genre, duration)
        elif args[0] == 'suggestions':
            if len(args) == 1:
                print('Usage: yt suggestions URL')
                return
            suggestions = youtube_suggestions(args[1])
            if suggestions is None:
                return
            print(f'Autoplay video: {suggestions["autoplay_url"]}')
            for video in suggestions['suggestions']:
                print()
                for k, v in video.items():
                    print(f'{k.capitalize().replace("_", " ")}: {v}')
        else:
            print(f"Invalid command: {args[0]}")
            self.show_usage(self.do_yt)

    do_yt.commands = ['search', 'info', 'download', 'suggestions']
    do_yt.url_parts = ['https://youtu.be/', 'ytsearch5:']

    def complete_yt(self, text, line, begidx, endidx):
        index_dict = {
            1: self.do_yt.commands,
            2: self.do_yt.url_parts,
        }
        return self.index_based_complete(text, line, begidx, endidx,
                                         index_dict=index_dict)

    def save_state(self):
        with open(self.state_file, 'w') as f:
            json.dump({
                'playlist': self.list_player.media_list,
                'settings': {name: str(settable.get_value())
                             for name, settable in self.settables.items()},
                'index': self.list_player.media_list_index,
            }, f)

    def restore_state(self, state):
        for track in state['playlist']:
            self.list_player.add(track)
        for setting, value in state['settings'].items():
            if setting in self.settables:
                self.settables[setting].set_value(str(value))
        self.list_player.media_list_index = state['index']

    def check_play_mode(self, param_name, old, new):
        self.list_player.at_end_action = new

    def check_randomtrackpath(self, param_name, old, new):
        self.list_player.random_track_path_pattern = new

    def check_httpserver(self, param_name, old, new):
        if new and not old:
            self.launch_http_server()
        elif not new and old:
            self.httpd.shutdown()

    def launch_http_server(self):
        try:
            self.httpd = run_server(self.list_player, self)[0]
        except OSError:
            print("An error occured launching http server, it won't be "
                  "available.")
            if self.debug:
                traceback.print_exc()
            self.httpserver = False

    def on_exit(self) -> None:
        if self.savestate:
            self.save_state()
        self.list_player.stop()
        if self.httpd is not None:
            self.httpd.shutdown()

    def show_usage(self, method):
        if not hasattr(method, cmd2.constants.CMD_ATTR_ARGPARSER):
            print(method.__doc__.splitlines()[0])
            if hasattr(method, 'commands'):
                print(f'Subcommands: {", ".join(method.commands)}')
        else:
            method.argparser.print_usage()

    def do_pdb(self, line):
        """(Debugger)"""
        lp = list_player = self.list_player  # NOQA: F841
        mc = musiccli = self  # NOQA: F841
        try:
            import ipdb          # NOQA: T100
            ipdb.set_trace()     # NOQA: T100
        except ImportError:
            breakpoint()         # NOQA: T100
