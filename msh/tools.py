#! /usr/bin/env python3

import argparse
import glob
import json
import os
from pathlib import Path
import re
import time
from typing import Any
import urllib.request

import vlc

try:
    import yt_dlp
except ImportError:
    yt_dlp = None

from .settings import ALNUM_CHARS, YTDL_OPTS

YOUTUBE_VIDEO_REGEX = re.compile(
    r'^(?:https?://|//)?(?:(?:www\.|m\.)?youtube\.com/(?:watch\?v=|v/|embed/)'
    r'|youtu.be/)(?P<id>[^#&?]+)([?&#].+)?$')

METADATA_STRING_ENUM_MAPPING = {
    'track_number': vlc.Meta.TrackNumber,
    'title': vlc.Meta.Title,
    'genre': vlc.Meta.Genre,
    'artist': vlc.Meta.Artist,
    'album': vlc.Meta.Album,
    'year': vlc.Meta.Date,
    'comment': vlc.Meta.Description,
}
METADATA_ENUM_STRING_MAPPING = {
    v: k for k, v in METADATA_STRING_ENUM_MAPPING.items()
}


def human_format(num: float | int) -> str:
    num = float(f'{num:.3g}')
    magnitude = 0
    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000.0
    return '{}{}'.format(f'{num:f}'.rstrip('0').rstrip('.'),
                         ['', 'K', 'M', 'Md', 'T'][magnitude])


def add_slash_if_dir(fn: str) -> str:
    if fn[-1] != '/' and os.path.isdir(fn):
        return fn + '/'
    else:
        return fn


def secs_to_hh_mm_ss(secs: int) -> str:
    mm, ss = divmod(secs, 60)
    if mm > 59:
        hh, mm = divmod(mm, 60)
        l = [hh, mm, ss]
    else:
        l = [mm, ss]
    l = [str(int(t)) if t >= 10 else '0' + str(int(t)) for t in l]
    return ':'.join(l)


def ytdl_extract_info(url: str, *, download=False) -> dict:
    if yt_dlp is None:
        print('yt-dlp is not installed: cannot play YouTube videos')
        # Return id for YouTube videos (they may be downloaded locally)
        if (match := YOUTUBE_VIDEO_REGEX.match(url)):
            return {'id': match['id']}
        return {}

    with yt_dlp.YoutubeDL(YTDL_OPTS) as ytdl:
        try:
            info = ytdl.extract_info(url, download)
        except yt_dlp.utils.DownloadError:
            print(f'An error occured trying to get the information of { url }')
            # Return id for YouTube videos (they may be downloaded locally)
            if (match := YOUTUBE_VIDEO_REGEX.match(url)):
                return {'id': match['id'], 'error': True}
            return {'error': True}

    info = {**info, 'error': False}

    return info


def _to_media(file: str | Path | vlc.Media) -> (tuple[str | Path, vlc.Media]
                                                | tuple[str | Path, None]):
    if isinstance(file, (str, Path)):
        filename = file
        if not os.path.exists(filename):
            print(f'"{ filename }" file does not exist')
            return filename, None
        media = vlc.Media(file)
        media.parse()
    elif isinstance(file, vlc.Media):
        media = file
        filename = urllib.parse.unquote(media.get_mrl())[len('file://'):]
        if not media.is_parsed():
            media.parse()
    else:
        raise TypeError('file must be of type str, Path or vlc.Media, not '
                        f'{type(file)}')
    return filename, media


def get_local_media_info(file: str | Path | vlc.Media) -> dict[str, Any]:
    filename, media = _to_media(file)
    if media is None:
        return None

    # We don't want any KeyError
    return {
        'filename': filename,
        **{k: media.get_meta(v) or '' for k, v in
           METADATA_STRING_ENUM_MAPPING.items()},
        'duration': media.get_duration() / 1000,
    }


def set_local_media_info(file: str | Path | vlc.Media,
                         info: dict[str, str]) -> None:
    filename, media = _to_media(file)
    if media is None:
        return None

    for k, v in info.items():
        media.set_meta(METADATA_STRING_ENUM_MAPPING[k], v)
    return media.save_meta()


def get_media_info(mrl: str) -> dict[str, Any]:
    if mrl.startswith(('http://', 'https://')):
        info = ytdl_extract_info(mrl)
        return {
            'title': info.get('title', 'No title'),
            'artist': info.get('uploader', 'No uploader found'),
            'duration': info.get('duration'),
            'album': 'YouTube',
            'genre': 'YouTube',
            'tracknumber': '',
        }
    else:
        return get_local_media_info(mrl)


def volume_value(val: str) -> int:
    """Used as type for value argument of volume command. Checks if it is
    between 0 and 200."""
    ivalue = int(val)
    if not 0 < ivalue < 200:
        raise argparse.ArgumentTypeError(
            f'{ val } is not an integer between 0 and 200')
    return ivalue


instance = None


def youtube_suggestions(url: str) -> dict:
    global instance        # NOQA: PLW0603
    if instance is None:
        try:
            response = urllib.request.urlopen(INVIDIOUS_INSTANCES_URL)
        except urllib.error.HTTPError:
            print("Couldn't find an invidious instance")
            return None
        instance = json.loads(response.read())[0][1]['uri']
    if not (match := YOUTUBE_VIDEO_REGEX.match(url)):
        print(f"Couldn't find id of {url}")
        return None
    id_ = match.groups()[0]
    try:
        response = urllib.request.urlopen(
            f'{instance}/api/v1/videos/{id_}?fields=recommendedVideos')
    except urllib.error.HTTPError as error:
        try:
            suggestions = json.loads(error.read())["recommendedVideos"]
        except json.JSONDecodeError:
            print(f'Error: {error}')
            return None
    else:
        suggestions = json.loads(response.read())["recommendedVideos"]

    return {
        'autoplay_url': YOUTUBE_URL.format(id=suggestions[0]['videoId']),
        'suggestions': [
            {
                'url': YOUTUBE_URL.format(id=video['videoId']),
                'title': video['title'],
                'author': video['author'],
                'duration': secs_to_hh_mm_ss(video['lengthSeconds'])
            } for video in suggestions
        ]
    }
