#! /usr/bin/env python3

import mimetypes
import os
import sqlite3
import threading
import traceback

from .tools import get_local_media_info


class TagsDB:
    """Create, index and search in an sqlite db for music"""
    def __init__(self, db_path):
        self.connection = sqlite3.connect(db_path, check_same_thread=False)
        # We're using check_same_thread=False, so we need to make sure there is
        # no write from different threads
        self._write_thread = threading.current_thread()
        cursor = self.connection.cursor()
        cursor.execute(
            'CREATE TABLE IF NOT EXISTS tags (path TEXT UNIQUE, title TEXT, '
            'artist TEXT, album TEXT, track TEXT, genre TEXT, '
            'duration REAL, last_modified INTEGER)')
        self.columns_name = ['path', 'title', 'artist', 'album', 'track',
                             'genre', 'duration', 'last_modified']
        self.connection.commit()

    def get_from_path(self, path):
        """Search metadata in db for path path and add it if it not in the db
        yet"""
        abspath = os.path.abspath(path)
        cursor = self.connection.cursor()
        cursor.execute('SELECT * FROM tags WHERE path = ?', [abspath])
        db_tags = cursor.fetchone()
        if db_tags is None:
            tags = get_local_media_info(abspath)
            all_tags = [tags.get(key, '') for key in self.columns_name[1:7]]
            last_modified = int(os.stat(abspath).st_mtime)
            if self._write_thread == threading.current_thread():
                cursor.execute(
                    'INSERT INTO tags VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
                    [abspath, *all_tags, last_modified])
            self.connection.commit()
            return tags
        else:
            return {key: val for key, val in
                    zip(self.columns_name[1:7], db_tags[1:7])}

    def scan(self, path, *, interactive=False):
        if self._write_thread != threading.current_thread():
            raise sqlite3.ProgrammingError(
                "Trying to run a scan from a different thread")
        for root, _, files in os.walk(path):
            for f in files:
                abspath = os.path.join(os.path.abspath(root), f)
                print('Filename:', abspath)
                mimetype = mimetypes.guess_type(abspath)
                if mimetype[0] is None or not mimetype[0].startswith('audio/'):
                    print('Not an audio file, skipping')
                    print()
                    continue
                cursor = self.connection.cursor()
                cursor.execute('SELECT * FROM tags WHERE path = ?', [abspath])
                db_tags = cursor.fetchone()
                try:
                    mtime = os.stat(abspath).st_mtime
                except OSError:
                    print(
                        f'Error getting last modification time of {abspath}\n')
                    continue
                except:   # NOQA: E722
                    traceback.print_exc()
                    print()
                    if interactive:
                        cont = input('Do you want to continue? [Y/n]')
                        if len(cont) > 0 and cont[0].lower() != 'y':
                            return
                    continue

                if db_tags is not None:
                    print('Already exists')
                    if not mtime > db_tags[7]:
                        print('Up to date\n')
                        continue
                try:
                    tags = get_local_media_info(abspath)
                    all_tags = [tags.get(key, '') for key in
                                self.columns_name[1:7]]
                except:  # NOQA: E722
                    traceback.print_exc()
                    if interactive:
                        cont = input('Do you want to continue? [Y/n]')
                        if len(cont) > 0 and cont[0].lower() != 'y':
                            return
                    all_tags = ['error'] * 6
                if db_tags is None:
                    cursor.execute(
                        'INSERT INTO tags VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
                        [abspath, *all_tags, mtime])
                else:
                    fields = ','.join(f'{field}=?' for field in
                                      self.columns_name[1:])
                    cursor.execute(f'UPDATE tags SET {fields} WHERE path = ?',
                                   [*all_tags, mtime, abspath])
                    print('Updated')
                self.connection.commit()
                print('Title:', all_tags[0])
                print('Artist:', all_tags[1])
                print('Album:', all_tags[2])
                print()

    def add_file(self, path, title, artist, album, track, genre, duration,
                 mtime=None):
        """Add a file to db"""
        if self._write_thread != threading.current_thread():
            raise sqlite3.ProgrammingError(
                "Trying to add a file from a different thread")
        if mtime is None:
            mtime = os.stat(path).st_mtime
        cursor = self.connection.cursor()
        cursor.execute('INSERT INTO tags VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
                       [os.path.abspath(path), title, artist, album, track,
                        genre, duration, mtime])
        self.connection.commit()

    def search(self, query='%', title='%', artist='%', album='%', path='%',
               limit=None):
        """Search db with some arguments"""
        sql = ('SELECT path FROM tags WHERE (title LIKE ? OR artist LIKE ? OR '
               'album LIKE ?) AND title LIKE ? AND artist LIKE ? AND '
               'album LIKE ? AND path LIKE ?;')

        args = [f'%{arg}%' if '%' not in arg and '_' not in arg else arg
                for arg in (query, query, query, title, artist, album, path)]
        if limit is not None:
            sql = sql[:-1] + ' LIMIT ?;'
            args.append(limit)
        cursor = self.connection.cursor()
        cursor.execute(sql.strip(), args)
        return cursor.fetchall()

    def random_track(self, path='%'):
        cursor = self.connection.cursor()
        cursor.execute("SELECT path FROM tags WHERE path LIKE ? ORDER BY "
                       "RANDOM() LIMIT 1", (path,))
        result = cursor.fetchone()
        if result is None or len(result) == 0:
            return None
        return result[0]
