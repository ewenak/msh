#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Adapted from https://gist.github.com/kwk/5387c0e8d629d09f93665169879ccb86

---
The httpserver module contains an easy and ready-to-use HTTP file server.
o ServeDirectoryWithHTTP: Spawns an HTTP file server in a separate thread.
"""

from functools import partial
from http import HTTPStatus
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import os
import tempfile
from threading import Thread, current_thread
from urllib.parse import parse_qs

from .settings import SERVER_ADDRESS, SERVER_LOG_FILENAME


class HTTPRequestHandler(BaseHTTPRequestHandler):
    def __init__(self, *args, list_player=None, musiccli=None, log_file,
                 **kwargs):
        self.list_player = list_player
        self.musiccli = musiccli
        self.log_file = log_file
        super().__init__(*args, **kwargs)

    def process_response(self):
        data = self.get_data()
        if self.path.startswith('/cmd/'):
            if data.get('cmd') is None or not len(data) == 1:
                self.send_error(HTTPStatus.BAD_REQUEST)
                return

            _xprint(data, log_file=self.log_file)

            statement = self.musiccli._input_line_to_statement(data['cmd'])
            if (statement.output or statement.pipe_to or statement.command
                    not in self.musiccli.http_allowed_commands):
                self.send_error(HTTPStatus.BAD_REQUEST)
                return

            fd, filename = tempfile.mkstemp(prefix="msh-server-")
            os.close(fd)
            self.musiccli.onecmd_plus_hooks(f'{ data["cmd"] } > {filename}')
            self.send_content(filename)
        elif self.path == '/status/':
            if not len(data) == 0:
                self.send_error(HTTPStatus.BAD_REQUEST)
                return
            status = {
                'state': str(self.list_player.player.get_state()),
                'time': self.list_player.player.get_time(),
                'length': self.list_player.player.get_length(),
                'filename': self.list_player.media_name,
                'mediainfo': self.list_player.media_info,
            }
            self.send_content(json.dumps(status), 'application/json')
        elif self.path == '/playlist/':
            if not len(data) == 0:
                self.send_error(HTTPStatus.BAD_REQUEST)
                return
            playlist = {
                'index': self.list_player.media_list_index,
                'playlist': [m for m in self.list_player.media_list]
            }
            self.send_content(json.dumps(playlist), 'application/json')
        else:
            self.send_error(HTTPStatus.NOT_FOUND)
            return

    def do_GET(self):
        self.method = 'GET'
        self.process_response()

    def do_POST(self):
        self.method = 'POST'
        self.process_response()

    def send_content(self, content: str, mimetype='text/plain'):
        content_as_bytes = content.encode('utf-8')
        self.send_response(HTTPStatus.OK)
        self.send_header('Content-Type', mimetype)
        self.send_header('Content-Length', str(len(content_as_bytes)))
        self.end_headers()
        self.wfile.write(content_as_bytes)

    def get_data(self):
        if self.method == 'POST':
            content = self.rfile.read(
                int(self.headers['Content-Length'])).decode('UTF-8')
        elif self.method == 'GET':
            if '?' not in self.path:
                return {}
            content = self.path.split('?', 1)[1]
        query = {k: v[0] for k, v in
                 parse_qs(content, keep_blank_values=True).items()}
        return query

    def log_message(self, format, *args):
        _xprint("%s - - [%s] %s\n" %
                (self.address_string(),
                 self.log_date_time_string(),
                 format % args), log_file=self.log_file)


def run_server(list_player, musiccli, server_address=SERVER_ADDRESS,
               server_class=HTTPServer, handler_class=HTTPRequestHandler):
    log_file = open(SERVER_LOG_FILENAME, 'a')
    # Handler needs to be callable, so to pass parameters to our handler, we
    # need to use partial
    handler = partial(HTTPRequestHandler, list_player=list_player,
                      musiccli=musiccli, log_file=log_file)
    httpd = HTTPServer(server_address, handler)
    # Block only for 0.5 seconds max
    httpd.timeout = 0.5
    # Allow for reusing the address
    # HTTPServer sets this as well but I wanted to make this more obvious.
    httpd.allow_reuse_address = True

    address = "http://%s:%d" % (httpd.server_name, httpd.server_port)

    _xprint("server about to listen on:", address, log_file=log_file)
    httpd.server_activate()

    def serve_forever(httpd):
        with httpd:  # to make sure httpd.server_close is called
            httpd.serve_forever()
            _xprint("server left infinite request loop", log_file=log_file)
        log_file.close()

    thread = Thread(target=serve_forever, args=(httpd, ))
    thread.setDaemon(True)
    thread.start()

    return httpd, address


def _xprint(*args, log_file=None, **kwargs):
    """Wrapper function around print() that prepends the current thread name"""
    if log_file is None:
        print("\r[", current_thread().name, "]",
              " ".join(map(str, args)), **kwargs)
    else:
        print("\r[", current_thread().name, "]",
              " ".join(map(str, args)), **kwargs, file=log_file)
