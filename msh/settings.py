#! /usr/bin/env python3

import platformdirs
import string
import os

ALNUM_CHARS = string.ascii_lowercase + string.ascii_uppercase + string.digits

APPNAME = 'msh'
AUTHOR_NAME = 'Ewe Nak'

CACHE_DIR = platformdirs.user_cache_path(APPNAME, appauthor=AUTHOR_NAME)
DATA_DIR = platformdirs.user_data_path(APPNAME, appauthor=AUTHOR_NAME)
LOGS_DIR = platformdirs.user_log_path(APPNAME, appauthor=AUTHOR_NAME)
CONFIG_DIR = platformdirs.user_config_path(APPNAME, appauthor=AUTHOR_NAME)

for directory in CACHE_DIR, DATA_DIR, LOGS_DIR, CONFIG_DIR:
    directory.mkdir(parents=True, exist_ok=True)

DOWNLOADED_VIDEOS_DIR = DATA_DIR / 'downloads'
TAGSDB_FILENAME = DATA_DIR / 'tagsdb.sqlite3'
STATE_FILENAME = CACHE_DIR / 'state.json'

VLC_LOGFILE = LOGS_DIR / f'vlc-{ os.getpid() }.log'
SERVER_LOG_FILENAME = LOGS_DIR / 'msh-server.log'

MSHRC_FILENAME = CONFIG_DIR / 'rc'

SERVER_ADDRESS = ('localhost', 1113)

YTDL_OPTS = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }, {
        'key': 'FFmpegMetadata',
    }],
    'default_search': 'ytsearch',
    'extract_flat': 'in_playlist',
    'outtmpl': (DOWNLOADED_VIDEOS_DIR / '%(id)s.%(ext)s').as_posix(),
}
