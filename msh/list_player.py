#! /usr/bin/env python3

import glob
import os
from threading import Timer
import urllib
import vlc

from .settings import DOWNLOADED_VIDEOS_DIR
from .tools import (get_media_info, ytdl_extract_info, YOUTUBE_VIDEO_REGEX,
                    youtube_suggestions)


ALLOWED_AT_END_ACTIONS = ('repeat', 'random', 'stop', 'yt_suggestions')


class ListPlayer:
    def __init__(self, instance, tags_db, media_list=None,
                 at_end_action="stop"):
        self.instance = instance

        # "stop", "repeat", "random" or "yt_suggestions"
        self.at_end_action = at_end_action
        # SQL LIKE pattern
        self.random_track_path_pattern = "%"

        self.media_list = []
        if media_list is not None:
            self.add(*media_list)

        self._media_list_index = 0
        self.media_name = None
        self.media = None
        self.media_info = {}

        self.tags_db = tags_db

        self.player = instance.media_player_new()
        self.event_manager = self.player.event_manager()

        def media_ended(event):
            Timer(0, self.next).start()

        self.event_manager.event_attach(vlc.EventType.MediaPlayerEndReached,
                                        media_ended)

    def __len__(self):
        return len(self.media_list)

    def __getitem__(self, index):
        return self.media_list[index]

    def __delitem__(self, index):
        del self.media_list[index]

    @property
    def volume(self):
        return self.player.audio_get_volume()

    @volume.setter
    def volume(self, value):
        return self.player.audio_set_volume(value)

    @property
    def media_list_index(self):
        return self._media_list_index

    @media_list_index.setter
    def media_list_index(self, value):
        if not isinstance(value, int):
            raise TypeError("media_list_index must be int")
        was_playing = self.is_playing()
        if was_playing:
            self.stop()
        self._media_list_index = value
        if was_playing:
            self.play()

    def _get_media(self, media_name):
        if '://' in media_name:
            info = ytdl_extract_info(media_name)

            if len(info) == 0:
                return None

            filenames = glob.glob(
                f'{os.path.join(DOWNLOADED_VIDEOS_DIR, info["id"])}.*')
            if len(filenames) > 0:
                return self.instance.media_new(filenames[0])

            if info.get('vcodec') == 'none':
                return self.instance.media_new(info['url'])

            formats = info.get('formats', [])
            for format_info in formats:
                if format_info['vcodec'] == 'none':
                    return self.instance.media_new(format_info['url'])
            return None
        return self.instance.media_new(media_name)

    def add(self, *filenames, position=None):
        if position == 'next':
            position = self._media_list_index + 1
        elif position in ('last', len(self.media_list)):
            position = None

        for filename in filenames:
            if isinstance(filename, vlc.Media):
                filename = filename.get_mrl()
            if filename.startswith('file://'):
                filename = urllib.parse.unquote(filename)[len('file://'):]

            if '://' in filename:
                if 'playlist' in urllib.parse.urlsplit(filename).path:
                    playlist = ytdl_extract_info(filename)
                    if (not playlist['_type'] == 'playlist' or not
                            playlist['entries'][0]['_type'] == 'url'):
                        continue
                    position = self.add(
                        *(vid['url'] for vid in playlist['entries']),
                        position=position)
                    continue
            if '://' not in filename:
                filename = os.path.abspath(filename)
                if filename.endswith('.m3u'):
                    with open(filename, 'r') as file:
                        lines = [line.strip('\n') for line in file.readlines()]
                        position = self.add(*lines, position=position)
                        continue

            if position is not None:
                self.media_list.insert(position, filename)
                position += 1
            else:
                self.media_list.append(filename)

        return position

    def is_playing(self):
        return self.player.get_state() in set(
            [vlc.State.Opening, vlc.State.Buffering, vlc.State.Playing])

    def play(self):
        # Add another track just before getting to the final track to be able
        # to change it
        if self._media_list_index + 1 >= len(self.media_list):
            if self.at_end_action == 'random':
                path = self.tags_db.random_track(
                    path=self.random_track_path_pattern)
                if path is None:
                    path = self.tags_db.random_track(
                        path=f'%{self.random_track_path_pattern}%')
                    if path is None:
                        # We didn't find any track, even adding wildcards (%
                        # for SQL LIKE), let's just ignore the pattern
                        path = self.tags_db.random_track()
                if path is not None:
                    self.add(path)
            elif self.at_end_action == 'yt_suggestions':
                youtube_tracks = [url for url in self.media_list if
                                  YOUTUBE_VIDEO_REGEX.match(url)]
                if not len(youtube_tracks) == 0:
                    youtube_url = next(reversed(youtube_tracks))
                    suggestions = youtube_suggestions(youtube_url)
                    if suggestions is not None:
                        self.add(suggestions['autoplay_url'])

        if self._media_list_index >= len(self.media_list):
            if self.at_end_action == "repeat":
                self._media_list_index = 0
            else:
                return

        self.media_name = self.media_list[self._media_list_index]

        self.media = self._get_media(self.media_name)
        if self.media is None:
            # Skip unavailable medias, most notably unavailable videos on
            # YouTube that are present in a playlist
            self.next()
            return

        self.media_info = get_media_info(self.media_name)
        if self.player is not None:
            if self.is_playing():
                return
        else:
            self.player = self.instance.media_player_new()
        self.player.set_media(self.media)
        self.player.play()

    def next(self):
        self._media_list_index += 1
        self.play()

    def previous(self):
        self._media_list_index -= 1
        self.play()

    def pause(self):
        if self.player is not None:
            self.player.pause()

    def stop(self):
        if self.player is not None:
            self.player.stop()

    def reset(self):
        self.media_list = []
        self._media_list_index = 0

    def get_duration(self):
        if self.is_playing() or self.player.get_state() == vlc.State.Paused:
            return self.player.get_length() / 1000
        return None

    def get_time(self):
        if self.is_playing() or self.player.get_state() == vlc.State.Paused:
            return self.player.get_time() / 1000
        return None

    def set_time(self, seconds):
        self.player.set_time(int(seconds * 1000))
